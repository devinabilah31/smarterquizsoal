package com.devis.smarterquizsoal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

public class QuizActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        ImageView toolbarBack = findViewById(R.id.toolbar_back);
        ImageView toolbarMenu = findViewById(R.id.toolbar_menu);
        TextView toolbarText = findViewById(R.id.toolbar_text);

        toolbarBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Toast.makeText(QuizActivity.this, "Back", Toast.LENGTH_SHORT).show();
            }
        });

        toolbarMenu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Toast.makeText(QuizActivity.this, "Menu", Toast.LENGTH_SHORT).show();
            }
        });

        toolbarText.setText("Quiz");
    }
}