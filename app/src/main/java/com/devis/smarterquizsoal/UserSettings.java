package com.devis.smarterquizsoal;

public class UserSettings {

    private User user;
    private UserAccountSettings settings;

    public UserSettings(User user, UserAccountSettings settings){
        this.user = user;
        this.settings = settings;
    }

    public UserSettings(){

    }

    public User getUser(){
        return user;
    }

    public void setUser(){
        this.user = user;
    }

    public UserAccountSettings getSettings(){
        return settings;
    }

    public void setSettings(UserAccountSettings settings){
        this.settings = settings;
    }

    @Override
    public String toString(){
        return "UserSettings(" +
                "users" + user +
                ", settings" + settings +
                '}';
    }
}
