package com.devis.smarterquizsoal;

import android.content.Context;
import android.util.AttributeSet;

import java.util.jar.Attributes;

import androidx.appcompat.widget.AppCompatImageView;

public class RectangleImageView extends AppCompatImageView {

    public RectangleImageView(Context context){
        super(context);
    }

    public RectangleImageView(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    public RectangleImageView(Context context, AttributeSet attrs, int defStyleAttr){
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
